import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_flutter_demo/locator.dart';
import 'package:firebase_flutter_demo/models/user.dart';
import 'package:firebase_flutter_demo/services/analytics_service.dart';
import 'package:firebase_flutter_demo/services/firestore_service.dart';
import 'package:flutter/foundation.dart';

class AuthenticationService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FireStoreService _fireStoreService = locator<FireStoreService>();
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  User _currentUser;

  User get currentUser => _currentUser;

  Future loginWithEmail({
    @required String email,
    @required String password,
  }) async {
    try {
      var authResult = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      await _populateCurrentUser(authResult.user);
      return authResult.user != null;
    } catch (e) {
      return e.message;
    }
  }

  Future signUpWithEmail({
    @required String email,
    @required String password,
    @required String fullName,
    @required String role,
  }) async {
    try {
      var authResult = await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      _currentUser = User(
        id: authResult.user.uid,
        email: email,
        fullName: fullName,
        userRole: role,
      );
      await _fireStoreService.createUser(_currentUser);
      // set the user id on the analytics service
      await _analyticsService.setUserProperties(
          userId: authResult.user.uid, userRole: _currentUser.userRole);
      await _populateCurrentUser(authResult.user);
      return authResult.user != null;
    } catch (e) {
      return e.message;
    }
  }

  Future<bool> isUserLoggedIn() async {
    var user = await _firebaseAuth.currentUser();
    await _populateCurrentUser(user); // Populate the user information
    return user != null;
  }

  Future _populateCurrentUser(FirebaseUser user) async {
    if (user != null) {
      _currentUser = await _fireStoreService.getUser(user.uid);
      // set the user id on the analytics service
      await _analyticsService.setUserProperties(
          userId: user.uid, userRole: _currentUser.userRole);
    }
  }

  Future logOut() async {
    try {
      await _firebaseAuth.signOut();
      return true;
    } catch (e) {
      return e.message;
    }
  }
}
