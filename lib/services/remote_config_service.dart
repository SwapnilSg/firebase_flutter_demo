import 'package:firebase_remote_config/firebase_remote_config.dart';

const String _ShowMainBanner = "show_main_banner";

class RemoteConfigService {
  final RemoteConfig _remoteConfig;
  static RemoteConfigService _instance;
  final defaults = <String, dynamic>{_ShowMainBanner: "Default"};

  String get showMainBanner => _remoteConfig.getString(_ShowMainBanner);

  static Future<RemoteConfigService> getInstance() async {
    if (_instance == null) {
      _instance = RemoteConfigService(
        remoteConfig: await RemoteConfig.instance,
      );
    }
    return _instance;
  }

  RemoteConfigService({RemoteConfig remoteConfig})
      : _remoteConfig = remoteConfig;

  Future initialise() async {
    try {
      await _remoteConfig.setDefaults(defaults);
      await _fetchAndActivate();
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print('Remote config fetch throttled: $exception');
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

  Future _fetchAndActivate() async {
    await _remoteConfig.fetch(expiration:Duration(seconds: 0));
    await _remoteConfig.activateFetched();
  }
}
