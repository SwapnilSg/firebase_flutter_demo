import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_flutter_demo/models/post.dart';
import 'package:firebase_flutter_demo/models/user.dart';
import 'package:flutter/services.dart';

class FireStoreService {
  final CollectionReference _usersCollectionReference =
      Firestore.instance.collection("users");
  final CollectionReference _postsCollectionReference =
      Firestore.instance.collection('posts');
  final StreamController<List<Post>> _postsController =
      StreamController<List<Post>>.broadcast();
  DocumentSnapshot _lastDocument;
  List<List<Post>> _allPagedResults = <List<Post>>[];
  bool _hasMorePosts = true;

  Future createUser(User user) async {
    try {
      await _usersCollectionReference.document(user.id).setData(user.toJson());
    } catch (e) {
      return e.message;
    }
  }

  Future getUser(String uid) async {
    try {
      var userData = await _usersCollectionReference.document(uid).get();
      return User.fromData(userData.data);
    } catch (e) {
      return e.message;
    }
  }

  Future addPost(Post post) async {
    try {
      await _postsCollectionReference.add(post.toMap());
      return true;
    } catch (e) {
      return e.toString();
    }
  }

  ///Once Off Request for Data
  Future getPostsOnceOff() async {
    try {
      var postDocuments = await _postsCollectionReference.getDocuments();
      if (postDocuments.documents.isNotEmpty) {
        return postDocuments.documents
            .map((snapshot) => Post.fromMap(snapshot.data, snapshot.documentID))
            .where((mappedItem) => mappedItem.title != null)
            .toList();
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }
      return e.toString();
    }
  }

  Stream listenToPostsRealTime() {
    // Register the handler for when the posts data changes
    _requestPosts();
    // Return the stream underlying our _postsController.
    return _postsController.stream;
  }

  void _requestPosts() {
    var pagePostsQuery = _postsCollectionReference.orderBy('title').limit(2);
    if (_lastDocument != null) {
      pagePostsQuery = pagePostsQuery.startAfterDocument(_lastDocument);
    }
    // If there's no more posts then bail out of the function
    if (!_hasMorePosts) return;
    var currentRequestIndex = _allPagedResults.length;

    pagePostsQuery.snapshots().listen((postsSnapshot) {
      if (postsSnapshot.documents.isNotEmpty) {
        var posts = postsSnapshot.documents
            .map((snapshot) => Post.fromMap(snapshot.data, snapshot.documentID))
            .where((mappedItem) => mappedItem.title != null)
            .toList();
        // Check if the page exists or not
        var pageExists = currentRequestIndex < _allPagedResults.length;

        // If the page exists update the posts for that page
        if (pageExists) {
          _allPagedResults[currentRequestIndex] = posts;
        }
        // If the page doesn't exist add the page data
        else {
          _allPagedResults.add(posts);
        }

        // Concatenate the full list to be shown
        var allPosts = _allPagedResults.fold<List<Post>>(<Post>[],
            (initialValue, pageItems) => initialValue..addAll(pageItems));

        //  Broadcast all posts
        _postsController.add(allPosts);
        // Save the last document from the results only if it's the current last page
        if (currentRequestIndex == _allPagedResults.length - 1) {
          _lastDocument = postsSnapshot.documents.last;
        }

        // Determine if there's more posts to request
        _hasMorePosts = posts.length == 2;
      }
    });
  }

  Future deletePost(String documentId) async {
    await _postsCollectionReference.document(documentId).delete();
  }

  Future updatePost(Post post) async {
    try {
      await _postsCollectionReference
          .document(post.documentId)
          .updateData(post.toMap());
      return true;
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }
      return e.toString();
    }
  }

  void requestMoreData() => _requestPosts();
}
