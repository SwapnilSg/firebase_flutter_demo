import 'dart:async';
import 'dart:io';

import 'package:firebase_flutter_demo/constants/route_names.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../locator.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final NavigationService _navigationService = locator<NavigationService>();
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future initialise() async {
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    var android = AndroidInitializationSettings('mipmap/ic_launcher');
    var iOS = IOSInitializationSettings();
    var platform = InitializationSettings(android: android, iOS: iOS);
    _flutterLocalNotificationsPlugin.initialize(platform,
        onSelectNotification:onSelectNotification);

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showNotification(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _serialiseAndNavigate(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
  }

  _showNotification(Map<String, dynamic> message) async {
    var android = AndroidNotificationDetails(
        'channel_id', 'CHANNEL_NAME', 'channelDescription');
    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android: android, iOS: iOS);
    await _flutterLocalNotificationsPlugin.show(

      0,
      "title",
      "TEST",
      platform,
    );

    //_serialiseAndNavigate(message);
  }

  void _serialiseAndNavigate(Map<String, dynamic> message) {
    var notificationData = message['data'];
    var view = notificationData['view'];

    if (view != null) {
      // Navigate to the create post view
      if (view == 'create_post') {
        _navigationService.navigateTo(CreatePostViewRoute);
      }
      // If there's no view it'll just open the app on the first view
    }
  }
  Future onSelectNotification(String payload) async {

  }
}
