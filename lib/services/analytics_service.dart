import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future setUserProperties({@required String userId, String userRole}) async {
    await _analytics.setUserId(userId);
    // Set the user_role
    await _analytics.setUserProperty(name: 'user_role', value: userRole);
  }

  Future logLogin() async => await _analytics.logLogin(loginMethod: 'email');

  Future logSignUp() async => await _analytics.logSignUp(signUpMethod: 'email');

  Future logPostCreated({bool hasImage}) async =>
      await _analytics.logEvent(name: 'create_post', parameters: {
        'has_image': hasImage,
      });
}
