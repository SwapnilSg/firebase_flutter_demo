import 'dart:io';

import 'package:image_picker/image_picker.dart';

class ImageSelector {
  Future<File> selectImage() async {
    var selectedImage =
        await ImagePicker().getImage(source: ImageSource.gallery);
    return File(selectedImage.path);
  }
}
