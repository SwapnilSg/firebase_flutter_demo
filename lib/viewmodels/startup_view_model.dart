import 'package:firebase_flutter_demo/constants/route_names.dart';
import 'package:firebase_flutter_demo/locator.dart';
import 'package:firebase_flutter_demo/services/authentication_service.dart';
import 'package:firebase_flutter_demo/services/dynamic_link_service.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:firebase_flutter_demo/services/push_notification_service.dart';
import 'package:firebase_flutter_demo/services/remote_config_service.dart';

import 'base_model.dart';

class StartUpViewModel extends BaseModel {
  final AuthenticationService _authenticationService =
  locator<AuthenticationService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final PushNotificationService _pushNotificationService =
  locator<PushNotificationService>();
  final DynamicLinkService _dynamicLinkService = locator<DynamicLinkService>();
  final RemoteConfigService _remoteConfigService = locator<RemoteConfigService>();


  Future handleStartUpLogic() async {
    await _pushNotificationService.initialise();
    await _remoteConfigService.initialise();
    var hasLoggedInUser = await _authenticationService.isUserLoggedIn();

    if (hasLoggedInUser) {
      await _dynamicLinkService.handleDynamicLinks();
      _navigationService.navigate(HomeViewRoute);
    } else {
      _navigationService.navigate(LoginViewRoute);
    }
  }
}
