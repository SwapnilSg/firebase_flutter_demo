import 'package:firebase_flutter_demo/constants/route_names.dart';
import 'package:firebase_flutter_demo/locator.dart';
import 'package:firebase_flutter_demo/services/analytics_service.dart';
import 'package:firebase_flutter_demo/services/authentication_service.dart';
import 'package:firebase_flutter_demo/services/dialog_service.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:flutter/foundation.dart';

import 'base_model.dart';

class SignUpViewModel extends BaseModel {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final AnalyticsService _analyticsService = locator<AnalyticsService>();
  String _selectedRole = 'Select a User Role';

  String get selectedRole => _selectedRole;

  setSelectedRole(String value) {
    _selectedRole = value;
    notifyListeners();
  }

  Future signUp({
    @required String email,
    @required String password,
    @required String fullName,
  }) async {
    setBusy(true);
    var result = await _authenticationService.signUpWithEmail(
        email: email,
        password: password,
        fullName: fullName,
        role: selectedRole);
    setBusy(false);
    if (result is bool) {
      if (result) {
        _analyticsService.logSignUp();
        _navigationService.navigate(HomeViewRoute);
      } else {
        await _dialogService.showDialog(
          title: 'Sign Up Failure',
          description: 'General sign up failure. Please try again later',
        );
      }
    } else {
      await _dialogService.showDialog(
        title: 'Sign Up Failure',
        description: result,
      );
    }
  }
}
