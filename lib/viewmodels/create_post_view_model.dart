import 'dart:io';

import 'package:firebase_flutter_demo/models/post.dart';
import 'package:firebase_flutter_demo/services/analytics_service.dart';
import 'package:firebase_flutter_demo/services/cloud_storage_service.dart';
import 'package:firebase_flutter_demo/services/dialog_service.dart';
import 'package:firebase_flutter_demo/services/firestore_service.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:firebase_flutter_demo/utils/image_selector.dart';
import 'package:flutter/foundation.dart';

import '../locator.dart';
import 'base_model.dart';

class CreatePostViewModel extends BaseModel {
  final FireStoreService _firestoreService = locator<FireStoreService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final AnalyticsService _analyticsService = locator<AnalyticsService>();
  Post _editingPost;

  void setEditingPost(Post post) {
    _editingPost = post;
  }

  final ImageSelector _imageSelector = locator<ImageSelector>();
  final CloudStorageService _cloudStorageService =
      locator<CloudStorageService>();

  File _selectedImage;

  File get selectedImage => _selectedImage;

  Future selectImage() async {
    var tempImage = await _imageSelector.selectImage();
    if (tempImage != null) {
      _selectedImage = tempImage;
      notifyListeners();
    }
  }

  bool get _editing => _editingPost != null;

  Future addPost({@required String title}) async {
    setBusy(true);
    CloudStorageResult storageResult;

    if (!_editing && _selectedImage != null) {
      storageResult = await _cloudStorageService.uploadImage(
          imageToUpload: _selectedImage, title: title);
    }
    var result;
    if (!_editing) {
      result = await _firestoreService.addPost(Post(
        title: title,
        userId: currentUser.id,
        imageUrl: storageResult?.imageUrl ?? "",
        imageFileName: storageResult?.imageFileName ?? "",
      ));
      // Log the post created and check if the image is null
      await _analyticsService.logPostCreated(hasImage: _selectedImage != null);
    } else {
      result = await _firestoreService.updatePost(Post(
        userId: _editingPost.userId,
        title: title,
        documentId: _editingPost.documentId,
        imageUrl: _editingPost.imageUrl,
        imageFileName: _editingPost.imageFileName,
      ));
    }
// We need to add the current userId
    setBusy(false);

    if (result is String) {
      await _dialogService.showDialog(
        title: 'Could not add Post',
        description: result,
      );
    } else {
      await _dialogService.showDialog(
        title: 'Post successfully Added',
        description: 'Your post has been created',
      );
    }

    _navigationService.pop();
  }
}
