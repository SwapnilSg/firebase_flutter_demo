import 'package:firebase_flutter_demo/models/user.dart';
import 'package:firebase_flutter_demo/services/authentication_service.dart';
import 'package:firebase_flutter_demo/services/remote_config_service.dart';
import 'package:flutter/widgets.dart';

import '../locator.dart';

class BaseModel extends ChangeNotifier {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final RemoteConfigService _remoteConfigService =
      locator<RemoteConfigService>();

  String get showMainBanner => _remoteConfigService.showMainBanner;

  User get currentUser => _authenticationService.currentUser;

  bool _busy = false;

  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }
}
