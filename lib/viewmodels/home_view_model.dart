import 'package:firebase_flutter_demo/constants/route_names.dart';
import 'package:firebase_flutter_demo/models/post.dart';
import 'package:firebase_flutter_demo/services/authentication_service.dart';
import 'package:firebase_flutter_demo/services/cloud_storage_service.dart';
import 'package:firebase_flutter_demo/services/dialog_service.dart';
import 'package:firebase_flutter_demo/services/dynamic_link_service.dart';
import 'package:firebase_flutter_demo/services/firestore_service.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:share/share.dart';

import '../locator.dart';
import 'base_model.dart';

class HomeViewModel extends BaseModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final FireStoreService _fireStoreService = locator<FireStoreService>();
  final DialogService _dialogService = locator<DialogService>();
  final CloudStorageService _cloudStorageService =
      locator<CloudStorageService>();
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final DynamicLinkService _dynamicLinkService = locator<DynamicLinkService>();

  List<Post> _posts;

  List<Post> get posts => _posts;

  Future fetchPosts() async {
    setBusy(true);
    // TODO: Find or Create a TaskType that will automaticall do the setBusy(true/false) when being run.
    var postsResults = await _fireStoreService.getPostsOnceOff();
    setBusy(false);

    if (postsResults is List<Post>) {
      _posts = postsResults;
      notifyListeners();
    } else {
      await _dialogService.showDialog(
        title: 'Posts Update Failed',
        description: postsResults,
      );
    }
  }

  // Future navigateToCreateView() async {
  //   await _navigationService.navigateTo(CreatePostViewRoute);
  //   await fetchPosts();
  // }
  void listenToPosts() {
    setBusy(true);
    _fireStoreService.listenToPostsRealTime().listen((postsData) {
      List<Post> updatedPosts = postsData;
      if (updatedPosts != null && updatedPosts.length > 0) {
        _posts = updatedPosts;
        notifyListeners();
      }
      setBusy(false);
    });
  }

  Future deletePost(int index) async {
    var dialogResponse = await _dialogService.showConfirmationDialog(
      title: 'Are you sure?',
      description: 'Do you really want to delete the post?',
      confirmationTitle: 'Yes',
      cancelTitle: 'No',
    );
    if (dialogResponse.confirmed) {
      var postToDelete = _posts[index];
      setBusy(true);
      await _fireStoreService.deletePost(postToDelete.documentId);
      await _cloudStorageService.deleteImage(postToDelete.imageFileName);
      setBusy(false);
    }
  }

  void editPost(int index) {
    _navigationService.navigateTo(CreatePostViewRoute,
        arguments: _posts[index]);
  }

  Future navigateToCreateView() =>
      _navigationService.navigateTo(CreatePostViewRoute);

  Future logOut() async {
    setBusy(true);
    var result = await _authenticationService.logOut();
    setBusy(false);
    if (result is bool) {
      if (result) {
        _navigationService.navigate(LoginViewRoute);
      } else {
        await _dialogService.showDialog(
          title: 'Log Out Failure',
          description:
              'Couldn\'t logout at this moment. Please try again later',
        );
      }
    } else {
      await _dialogService.showDialog(
        title: 'Login Failure',
        description: result,
      );
    }
  }

  void requestMoreData() => _fireStoreService.requestMoreData();

  void shareDynamicLink(BuildContext context) async {
    var result = await _dynamicLinkService.createFirstPostLink();
    if (result != null) {
       await Share.share(result);
    } else {
      await _dialogService.showDialog(
        title: 'Share Failure',
        description: 'Couldn\'t share at this moment. Please try again later',
      );
    }
  }
}
