import 'package:firebase_flutter_demo/services/analytics_service.dart';
import 'package:firebase_flutter_demo/services/authentication_service.dart';
import 'package:firebase_flutter_demo/services/cloud_storage_service.dart';
import 'package:firebase_flutter_demo/services/dialog_service.dart';
import 'package:firebase_flutter_demo/services/dynamic_link_service.dart';
import 'package:firebase_flutter_demo/services/firestore_service.dart';
import 'package:firebase_flutter_demo/services/navigation_service.dart';
import 'package:firebase_flutter_demo/services/push_notification_service.dart';
import 'package:firebase_flutter_demo/services/remote_config_service.dart';
import 'package:firebase_flutter_demo/utils/image_selector.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => FireStoreService());
  locator.registerLazySingleton(() => CloudStorageService());
  locator.registerLazySingleton(() => ImageSelector());
  locator.registerLazySingleton(() => PushNotificationService());
  locator.registerLazySingleton(() => AnalyticsService());
  locator.registerLazySingleton(() => DynamicLinkService());
  var remoteConfigService = await RemoteConfigService.getInstance();
  locator.registerSingleton(remoteConfigService);
}
