import 'package:firebase_flutter_demo/models/post.dart';
import 'package:firebase_flutter_demo/ui/shared/ui_helpers.dart';
import 'package:firebase_flutter_demo/ui/widgets/input_field.dart';
import 'package:firebase_flutter_demo/viewmodels/create_post_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class CreatePostView extends StatelessWidget {
  final titleController = TextEditingController();
  final Post editingPost;
  final String postTitle;

  CreatePostView({
    Key key,
    this.editingPost,
    this.postTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CreatePostViewModel>.reactive(
      onModelReady: (model) {
        // If we have a post to edit, run edit logic
        if (editingPost != null) {
          titleController.text = editingPost?.title ?? '';

          model.setEditingPost(editingPost);
        }
        // if we have a title then set the title equal to the value passed in
        else if (postTitle != null) {
          titleController.text = postTitle;
        }
      },
      viewModelBuilder: () => CreatePostViewModel(),
      builder: (context, model, child) => Scaffold(
          floatingActionButton: FloatingActionButton(
            child: !model.busy
                ? Icon(Icons.add)
                : CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
            onPressed: () {
              // TODO: Add Post
              if (!model.busy)
                model.addPost(
                  title: titleController.text,
                );
            },
            backgroundColor:
                !model.busy ? Theme.of(context).primaryColor : Colors.grey[600],
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                verticalSpace(40),
                Text(
                  'Create Post',
                  style: TextStyle(fontSize: 26),
                ),
                verticalSpaceMedium,
                InputField(
                  placeholder: 'Title',
                  controller: titleController,
                ),
                verticalSpaceMedium,
                Text('Post Image'),
                verticalSpaceSmall,
                InkWell(
                  onTap: () => model.selectImage(),
                  child: Container(
                    height: 250,
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10)),
                    alignment: Alignment.center,
                    child: model.selectedImage == null
                        ? Text(
                            'Tap to add post image',
                            style: TextStyle(color: Colors.grey[400]),
                          )
                        // If we have a selected image we want to show it
                        : Image.file(model.selectedImage),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
