import 'package:firebase_flutter_demo/ui/widgets/creation_aware_list_item.dart';
import 'package:firebase_flutter_demo/ui/widgets/post_item.dart';
import 'package:firebase_flutter_demo/viewmodels/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => model.listenToPosts(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          child: !model.busy ? Icon(Icons.add) : CircularProgressIndicator(),
          onPressed: model.navigateToCreateView,
        ),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: SizedBox(
            height: 20,
            child: Image.asset('assets/images/title.png'),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.share),
                onPressed: () {
                  model.shareDynamicLink(context);
                }),
            IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  model.logOut();
                })
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              (model.showMainBanner != 'Default')
                  ? Container(
                      height: 80,
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 15),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 4,
                                offset: Offset(0, 4))
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      alignment: Alignment.center,
                      child: Text(
                        model.showMainBanner,
                        textAlign: TextAlign.center,
                      ),
                    )
                  : Container(),
              Expanded(
                child: model.posts != null
                    ? ListView.builder(
                        itemCount: model.posts.length,
                        itemBuilder: (context, index) => CreationAwareListItem(
                          itemCreated: () {
                            // when the item is created we request more data when it's the 20th index
                            if (index % 2 == 0) model.requestMoreData();
                          },
                          child: PostItem(
                            onEditPost: () => model.editPost(index),
                            post: model.posts[index],
                            onDeleteItem: () => model.deletePost(index),
                          ),
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(
                              Theme.of(context).primaryColor),
                        ),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
