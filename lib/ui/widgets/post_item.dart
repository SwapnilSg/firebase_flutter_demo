import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_flutter_demo/models/post.dart';
import 'package:flutter/material.dart';

class PostItem extends StatelessWidget {
  final Post post;
  final Function onDeleteItem;
  final Function onEditPost;

  const PostItem({Key key, this.post, this.onDeleteItem, this.onEditPost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: post.imageUrl != null ? null : 60,
      margin: const EdgeInsets.only(top: 20),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(blurRadius: 8, color: Colors.grey[200], spreadRadius: 3)
          ]),
      child: InkWell(
        onTap: () {
          if (onEditPost != null) onEditPost();
        },
        child: Row(
          children: <Widget>[
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  post.imageUrl != null && post.imageUrl != ''
                      ? SizedBox(
                          height: 250,
                          child: CachedNetworkImage(
                            imageUrl: post.imageUrl,
                            placeholder: (context, url) =>
                                Center(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                        )
                      : Container(),
                  Text(post.title),
                ],
              ),
            )),
            IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                if (onDeleteItem != null) {
                  onDeleteItem();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
